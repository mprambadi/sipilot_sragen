import sys
import shutil
import pandas as pd
import geopandas as gpd

file_name = sys.argv[1].rstrip('\n')
esri_wkt_data = sys.argv[2].rstrip('\n')
epsg_data = int(float(sys.argv[3].rstrip('\n')))

# set dataframe
df = pd.read_csv('./lib/python/{}.csv'.format(file_name))


# Obtain ESRI WKT
ESRI_WKT = esri_wkt_data
crs = {'init': 'epsg:4326'}

# create geopandas GeoDataFrame using pandas Data Frame
gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df['Longitude'], df['Lattitude']), crs=crs)

# convert tm3
gdf_tm3 = gdf.to_crs(epsg=epsg_data)

print(gdf_tm3)

# save he file as ESRI Shapefile
gdf_tm3.to_file(filename = '{}.shp'.format(file_name), driver='ESRI Shapefile')
