class HomepagesController < ApplicationController
  def index
    @space_pattern = Submission.done_space_pattern.limit(50)
    @validation_spatials = Submission.done_validation_spatials.limit(50)
    @validation_land_book = Submission.done_validation_land_book.limit(50)
  end

  def hello
    render json: { ip: request.ip }
  end

  def orde_number
  end

  def generatepdf
    respond_to do |format|
      format.html
      format.pdf do
        pdf = Prawn::Document.new
        pdf.text "Hello dunia"
        a = 1
        imagefile = "#{Rails.root}/app/assets/images/logo-sipilot-file.jpg"
        pdf.table([
          [
            { :content => "No", width: 15, align: :center },
            "Petugas",
            "Nama",
            "Keterangan", {
              :content => "", border_width: 0,
            }, {
              :content => "Sragen, #{I18n.l DateTime.now, locale: :id, format: :short}",
              border_width: 0,
              align: :right,
            },
          ],
          [{ :content => "1", width: 15, align: :center }, "Petugas <em>Ploting</em>", a == 2 ? "ok" : "okkkk", "", { :content => "", border_width: 0 }, {
            :image => imagefile,
            :image_height => 50,
            :image_width => 50,
            :rowspan => 3,
            border_width: 0,
            position: :right,
          }],
          [{ :content => "2", width: 15, align: :center }, "Validator", "", "", { :content => "", border_width: 0 }],
          [{ :content => "", border_width: 0, :colspan => 4 }],
        ], cell_style: {
             inline_format: true,
           }, width: pdf.bounds.width)

        send_data pdf.render, filename: "Order #1.pdf", type: "application/pdf", disposition: "inline"
      end
    end
  end

  def privacy_policy
  end
end
