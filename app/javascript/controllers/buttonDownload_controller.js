import { Controller } from 'stimulus';

export default class extends Controller {
    static targets = [
        "modalSHP"
    ]

    connect() {
        console.log('Button Download')
    }

    showModalSHP(event) {
        event.preventDefault()
        this.modalSHPTarget.classList.toggle('hidden')
    }
    
    hideModalSHP(event) {
        event.preventDefault()
        this.modalSHPTarget.classList.toggle('hidden')
    }
}
